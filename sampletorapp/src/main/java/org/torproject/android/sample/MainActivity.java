/*
 * Copyright (c) 2018 Michael Pöhn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.torproject.android.sample;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;
import hugo.weaving.DebugLog;
import info.guardianproject.netcipher.proxy.OrbotHelper;

public class MainActivity extends Activity {

    public static final String TAG = "MainActivity";

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.webview);
        final TextView statusTextView = findViewById(R.id.status);

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(context, intent.getStringExtra(OrbotHelper.EXTRA_STATUS), Toast.LENGTH_SHORT).show();
            }
        }, new IntentFilter(OrbotHelper.ACTION_STATUS));
        OrbotHelper.requestStartTor(this);

        GenericWebViewClient webViewClient = new GenericWebViewClient(this);
        webViewClient.setRequestCounterListener(new GenericWebViewClient.RequestCounterListener() {
            @Override
            public void countChanged(int requestCount) {
                statusTextView.setText("request count: " + requestCount + " - ");
            }
        });

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @DebugLog
            @Override
            public void onReceive(Context context, Intent intent) {
                String status = intent.getStringExtra(OrbotHelper.EXTRA_STATUS);
                Log.i(TAG, "onReceive: " + status + " " + intent);
                statusTextView.setText("Tor status: " + status);
            }
        };

        // run the BroadcastReceiver in its own thread
        HandlerThread handlerThread = new HandlerThread(receiver.getClass().getSimpleName());
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);
        registerReceiver(receiver, new IntentFilter(OrbotHelper.ACTION_STATUS), null, handler);


        webView.setWebViewClient(webViewClient);
        webView.loadUrl("https://check.torproject.org/");
    }

}
